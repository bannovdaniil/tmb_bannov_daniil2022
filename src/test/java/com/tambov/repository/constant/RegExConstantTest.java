package com.tambov.repository.constant;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class RegExConstantTest {

    @DisplayName("RegEx Check Upper Case First Letter")
    @ParameterizedTest
    @CsvSource(
            {
                    "IVAN, true",
                    "Ivan, true",
                    "ivan, false",
                    "1van, false",
                    "iVan, false",
                    "iVAN, false",
                    "*Ivan,false"
            }
    )
    void checkUpperCaseRegEx(String name, boolean expect) {
        Assertions.assertEquals(expect, name.matches(RegExConstant.UPPER_CASE_FIRST_LETTER_AZ));
    }

}