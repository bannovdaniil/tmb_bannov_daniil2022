package com.tambov.repository.validation;

import com.tambov.repository.constant.DateCheckerConstant;
import com.tambov.repository.constant.DateTimeFormatString;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mock;

import org.hibernate.validator.internal.util.annotation.AnnotationDescriptor;
import org.hibernate.validator.internal.util.annotation.AnnotationFactory;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class CheckDateValidatorTest {
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DateTimeFormatString.DATE_TIME_FORMATTER);
    private CheckDateValidator checkDateValidator;
    private CustomDateChecker customDateChecker;
    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    private CustomDateChecker createAnnotation(String value, String message) {
        final Map<String, Object> attrs = new HashMap<>();
        if (null != value) {
            attrs.put("value", value);
        }
        if (null != message) {
            attrs.put("message", message);
        }
        var desc = new AnnotationDescriptor
                .Builder<>(CustomDateChecker.class, attrs)
                .build();
        return AnnotationFactory.create(desc);
    }

    @BeforeEach
    void beforeEachTest() {
        checkDateValidator = new CheckDateValidator();
        customDateChecker = createAnnotation(DateCheckerConstant.CUSTOM_DATE
                , DateCheckerConstant.CUSTOM_DATE_ERROR_MESSAGE);
    }

    @DisplayName("Date after custom date")
    @ParameterizedTest
    @CsvSource({
            "2021-07-20 12:00:00, false, equal",
            "2021-07-19 12:00:00, false, before",
            "2022-07-21 12:00:00, true, after",
    })
    void isDateAfterCustomDate(String customDateTime, boolean isCorrect, String message) {
        LocalDateTime gameDate = LocalDateTime.parse(customDateTime, dateTimeFormatter);
        checkDateValidator.initialize(customDateChecker);

        Assertions.assertEquals(isCorrect
                , checkDateValidator.isValid(gameDate, constraintValidatorContext)
                , message);
    }
}