package com.tambov.repository.dao;

import com.tambov.repository.exception.GameNotFoundException;
import com.tambov.repository.mapper.DtoToGamesMapper;
import com.tambov.repository.mapper.GamesToDtoMapper;
import com.tambov.repository.model.Game;
import com.tambov.repository.model.Player;
import org.junit.jupiter.api.*;

import java.time.LocalDateTime;

abstract public class RepositoryGamesTest<T extends RepositoryGames> {
    private T repository;
    private Game game;
    private Player player1;
    private Player player2;
    private DtoToGamesMapper dtoToGamesMapper;
    private GamesToDtoMapper gamesToDtoMapper;

    public RepositoryGamesTest(T repository) {
        this.repository = repository;
    }

    @BeforeEach
    void beforeEach() throws Exception {
        repository.clearAllData();
        dtoToGamesMapper = new DtoToGamesMapper();
        gamesToDtoMapper = new GamesToDtoMapper();

        player1 = new Player("player1");
        player2 = new Player("player2");

        game = new Game(
                LocalDateTime.of(2022, 1, 1, 1, 0)
                , "name1"
                , 3
                , player1
                , player2
        );
        game.setGameId(1);
        game.setFieldSize(3);
    }

    @DisplayName("get Game by BAD Id")
    @Test
    void getGameByBadId() throws GameNotFoundException {
        Game gameFromDb = repository.create(gamesToDtoMapper.getGameDto(game));
        long gameId = gameFromDb.getGameId();

        Exception exception = Assertions.assertThrows(GameNotFoundException.class,
                () -> repository.getById(-1)
        );
        Assertions.assertEquals("getById. Not Found id", exception.getMessage());
    }

    @DisplayName("get Game byId")
    @Test
    void getGameById() throws GameNotFoundException {
        Game gameFromDb = repository.create(gamesToDtoMapper.getGameDto(game));
        long gameId = gameFromDb.getGameId();

        Game expectedGame = game;
        Player expectedPlayer1 = player1;
        Player expectedPlayer2 = player2;

        Game resultGame = repository.getById(gameId);

        Assertions.assertEquals(expectedGame, resultGame);
        Assertions.assertEquals(expectedPlayer1, resultGame.getPlayers().get(0));
        Assertions.assertEquals(expectedPlayer2, resultGame.getPlayers().get(1));
    }


    @DisplayName("Add Game record")
    @Test
    void add() throws GameNotFoundException {
        Game expectedGame = game;
        Player expectedPlayer1 = player1;
        Player expectedPlayer2 = player2;

        Game gameFromDb = repository.create(gamesToDtoMapper.getGameDto(game));
        long gameId = gameFromDb.getGameId();

        Game resultGame = repository.getById(gameId);

        Assertions.assertEquals(expectedGame, resultGame);
        Assertions.assertEquals(expectedPlayer1, resultGame.getPlayers().get(0));
        Assertions.assertEquals(expectedPlayer2, resultGame.getPlayers().get(1));
    }

    @Test
    @DisplayName("Update Method")
    void update() throws GameNotFoundException {
        game = repository.create(gamesToDtoMapper.getGameDto(game));
        long gameId = game.getGameId();

        String expectedName = "New Updated name";
        LocalDateTime expectedDateTime = LocalDateTime.of(2024, 1, 1, 1, 0);

        game.setGameName(expectedName);
        game.setGameDate(expectedDateTime);
        repository.update(gamesToDtoMapper.getGameDto(game));

        Game resultGame = repository.getById(gameId);

        Assertions.assertEquals(expectedName, resultGame.getGameName());
        Assertions.assertEquals(expectedDateTime, resultGame.getGameDate());
    }

    @Test
    @DisplayName("Remove Method")
    void remove() throws GameNotFoundException {
        Game gameFromDb = repository.create(gamesToDtoMapper.getGameDto(game));
        long gameId = gameFromDb.getGameId();

        repository.remove(gameId);

        Exception exception = Assertions.assertThrows(GameNotFoundException.class,
                () -> repository.getById(gameId)
        );
        Assertions.assertEquals("getById. Not Found id", exception.getMessage());
    }

}