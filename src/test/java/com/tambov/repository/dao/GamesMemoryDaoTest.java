package com.tambov.repository.dao;

import com.tambov.repository.mapper.DtoToGamesMapper;
import com.tambov.repository.mapper.GamesToDtoMapper;

class GamesMemoryDaoTest extends RepositoryGamesTest<GamesMemoryDao>{

    protected GamesMemoryDaoTest() {
        super(new GamesMemoryDao(new DtoToGamesMapper(), new GamesToDtoMapper()));
    }
}