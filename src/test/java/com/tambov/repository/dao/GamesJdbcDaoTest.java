package com.tambov.repository.dao;

import com.tambov.repository.mapper.DtoToGamesMapper;
import com.tambov.repository.mapper.GamesToDtoMapper;

class GamesJdbcDaoTest extends RepositoryGamesTest<GamesJdbcDao> {

    protected GamesJdbcDaoTest() throws Exception {
        super(new GamesJdbcDao(new DtoToGamesMapper(), new GamesToDtoMapper()));
    }
}