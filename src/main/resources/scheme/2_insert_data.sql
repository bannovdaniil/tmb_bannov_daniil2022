-- играющие
INSERT INTO players(player_name, player_rate) VALUES('Жираф', 15); -- 1
INSERT INTO players(player_name, player_rate) VALUES('Волк', 5);   -- 2
INSERT INTO players(player_name, player_rate) VALUES('Семен', 10); -- 3
INSERT INTO players(player_name, player_rate) VALUES('Иван', 5);   -- 4

SELECT * FROM players;

-- игры
INSERT INTO games(game_name, game_date, field_size) 
	VALUES ('первый отборочный', date('2021-01-01'), 3),  -- 1
               ('второй отборочный', date('2021-02-01'), 3),  -- 2
               ('турнир', date('2021-03-01'), 3), -- 3
               ('2022-04-13 игра', date('2022-04-03'), 3), -- 4
               ('универсал', date('2021-05-01'), 3), -- 5
               ('турнир 2х2', date('2021-06-01'), 4);  -- 6

SELECT * FROM games;

-- связь между игроками и играми
INSERT INTO game_players(game_id, player_id, game_char, player_status) 
	VALUES (1, 1, 'O', 0),
               (1, 2, 'X', 1);   -- 1
INSERT INTO game_players(game_id, player_id, game_char, player_status) 
	VALUES (2, 1, 'X', 0),
               (2, 3, 'O', 1);   -- 2
INSERT INTO game_players(game_id, player_id, game_char, player_status) 
	VALUES (3, 1, 'X', 0),
               (3, 4, 'O', 1);   -- 3
INSERT INTO game_players(game_id, player_id, game_char, player_status) 
	VALUES (4, 2, 'X', 2),
               (4, 3, 'O', 2);   -- 4
INSERT INTO game_players(game_id, player_id, game_char, player_status) 
	VALUES (5, 2, 'X', 2),
               (5, 4, 'O', 2);
INSERT INTO game_players(game_id, player_id, game_char, player_status) 
	VALUES (6, 3, 'X', 2),
               (6, 4, 'O', 2);

SELECT * FROM game_players;
-- ячейка
INSERT INTO cells(game_id, turn, x, y, status_char) 
	VALUES	(1, 3, 0, 0, 'X'), (1, 1, 0, 1, 'X'), (1, 5, 0, 2, 'X'),
		(1, 2, 1, 0, 'O'), (1, 0, 1, 1, 'O'), (1, 4, 1, 2, 'O');
INSERT INTO cells(game_id, turn, x, y, status_char) 
	VALUES	(2, 1, 0, 0, 'X'), (2, 4, 0, 1, 'O'),
		                   (2, 0, 1, 1, 'O'), (2, 3, 1, 2, 'X'),
		                   (2, 2, 2, 1, 'O');
INSERT INTO cells(game_id, turn, x, y, status_char) 
	VALUES	                                      (3, 2, 0, 2, 'O'),
		(3, 3, 1, 0, 'X'), (3, 0, 1, 1, 'O'), (3, 1, 1, 2, 'X'),
		(3, 4, 2, 0, 'O');
INSERT INTO cells(game_id, turn, x, y, status_char) 
	VALUES	(4, 3, 0, 0, 'X'), (4, 2, 0, 1, 'O'), (4, 7, 0, 2, 'X'),
		(4, 1, 1, 0, 'X'), (4, 5, 1, 1, 'X'), (4, 6, 1, 2, 'O'),
		(4, 0, 2, 0, 'O'), (4, 4, 2, 1, 'O'), (4, 8, 2, 2, 'O');
INSERT INTO cells(game_id, turn, x, y, status_char) 
	VALUES	(5, 8, 0, 0, 'O'), (5, 7, 0, 1, 'X'), (5, 4, 0, 2, 'O'),
		(5, 2, 1, 0, 'O'), (5, 0, 1, 1, 'O'), (5, 3, 1, 2, 'X'),
		(5, 5, 2, 0, 'X'), (5, 6, 2, 1, 'O'), (5, 1, 2, 2, 'X');
INSERT INTO cells(game_id, turn, x, y, status_char) 
	VALUES	(6,11, 0, 0, 'X'), (6, 0, 0, 1, 'O'), (6,13, 0, 2, 'X'), (6, 7, 0, 3, 'X'),
		(6, 9, 1, 0, 'X'), (6, 1, 1, 1, 'X'), (6,12, 1, 2, 'O'), (6, 6, 1, 3, 'O'),
		(6, 5, 2, 0, 'X'), (6, 3, 2, 1, 'X'), (6,14, 2, 2, 'O'), (6, 8, 2, 3, 'O'),
		(6, 4, 3, 0, 'O'), (6, 2, 3, 1, 'O'), (6,15, 3, 2, 'X'), (6,10, 3, 3, 'O');

SELECT * FROM cells;

