drop table IF EXISTS game_players;
drop table IF EXISTS cells;
drop table IF EXISTS players;
drop table IF EXISTS games;

CREATE TABLE IF NOT EXISTS games (
  game_id SERIAL PRIMARY KEY,
  game_name VARCHAR(30),
  game_date TIMESTAMP,
  field_size INT
);

CREATE TABLE IF NOT EXISTS players (
  player_id SERIAL PRIMARY KEY,
  player_name VARCHAR(30),
  player_rate INT
);

CREATE TABLE IF NOT EXISTS cells (
  cell_id SERIAL PRIMARY KEY,
  game_id INT REFERENCES games(game_id),
  turn INT,
  x INT,
  y INT,
  status_char VARCHAR(1) NOT NULL
);

CREATE TABLE IF NOT EXISTS game_players (
  game_players_id SERIAL PRIMARY KEY,
  game_id INT REFERENCES games(game_id),
  player_id INT REFERENCES players(player_id),
  game_char VARCHAR(1) NOT NULL,
  player_status INT
);
