-- самый простой все поля
SELECT * FROM game_players;
SELECT * FROM players;
SELECT * FROM cells;
SELECT * FROM games;

-- выборка по полям
SELECT cell_id, x, y, status_char FROM cells;

-- выборка с условием
SELECT * FROM cells
  WHERE game_id = 5;

-- выборка по вхождению в имя
SELECT game_id, game_name FROM games
WHERE game_name LIKE '%отбор%';

-- выборка по вхождению в имя
SELECT game_id, game_name FROM games
WHERE game_name LIKE '%отбор%';

-- получение игроков где победители играли 'O'
SELECT p.player_id AS id, p.player_name AS name, p.player_rate AS rate, gp.player_status AS status, gp.game_char AS char
FROM game_players AS gp
INNER JOIN players AS p
ON gp.player_status = 1 AND gp.game_char = 'O' AND p.player_id = gp.player_id;

-- получение игр где была ничья
SELECT DISTINCT ON (g.game_id) g.game_id AS id, g.game_name, gp.player_status AS status
FROM game_players AS gp
INNER JOIN games AS g
ON gp.player_status = 2 AND g.game_id = gp.game_id;

-- выбрать все ячейки для игры id=1 и упорядочить по ходам. 
SELECT *
from cells AS c
LEFT OUTER JOIN games AS g
ON c.game_id = g.game_id
WHERE g.game_id = 1
ORDER BY c.turn;

-- показать имена игр и количество ходов до победы менее 10
SELECT g.game_name, COUNT(*) AS turn_count
FROM games AS g
INNER JOIN cells AS c
ON c.game_id = g.game_id AND (SELECT COUNT(*) FROM cells AS cs WHERE cs.game_id = g.game_id)<10
GROUP BY g.game_name
ORDER BY g.game_name;

-- показать имена игр и количество ходов до победы менее 10
SELECT g.game_name, COUNT(*) AS turn_count
FROM games AS g
INNER JOIN cells AS c
ON c.game_id = g.game_id AND (SELECT COUNT(*) FROM cells AS cs WHERE cs.game_id = g.game_id)<10
GROUP BY g.game_name
ORDER BY g.game_name;

--  показать имена игр и количество ходов до победы менее 10
--  еще 1 вариант
SELECT t.game_name, countselect.count AS turn_count
FROM games AS t
INNER JOIN(
 SELECT game_id, COUNT(game_id) AS count
 FROM cells
 GROUP BY game_id
-- HAVING COUNT(game_id)<10
) AS countselect
ON countselect.game_id = t.game_id AND countselect.count<10;

