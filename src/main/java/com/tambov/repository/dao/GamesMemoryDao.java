package com.tambov.repository.dao;

import com.tambov.repository.dto.GameDto;
import com.tambov.repository.exception.GameNotFoundException;
import com.tambov.repository.mapper.DtoToGamesMapper;
import com.tambov.repository.mapper.GamesToDtoMapper;
import com.tambov.repository.model.Game;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;
import java.util.stream.Collectors;

public class GamesMemoryDao<T extends Game> implements RepositoryGames<T> {
    private final DtoToGamesMapper dtoToGamesMapper;
    private final GamesToDtoMapper gamesToDtoMapper;

    private final Map<Long, Game> games;
    @Getter
    @Setter
    private long countForNewGameId;

    @Autowired
    public GamesMemoryDao(DtoToGamesMapper dtoToGamesMapper, GamesToDtoMapper gamesToDtoMapper) {
        this.dtoToGamesMapper = dtoToGamesMapper;
        this.gamesToDtoMapper = gamesToDtoMapper;
        this.games = new HashMap<>();
        this.countForNewGameId = 0;
    }

    @Override
    public List<T> query(Specification specification) {
        return getAll().
                stream().
                map(game -> (T) game).
                filter(specification::isSpecified).
                collect(Collectors.toList());
    }

    public List<Game> getAll() {
        return new ArrayList<>(games.values());
    }

    @Override
    public Game create(GameDto gameDto) {
        Game game = dtoToGamesMapper.getDtoFromGame(gameDto);
        long newGameId = countForNewGameId;
        countForNewGameId++;
        game.setGameId(newGameId);
        games.put(newGameId, game);
        return game;
    }

    @Override
    public void remove(long gameId) throws GameNotFoundException {
        if (games.containsKey(gameId)) {
            games.remove(gameId);
        } else {
            throw new GameNotFoundException("remove. Not Found id");
        }
    }

    @Override
    public void update(GameDto gameDto) throws GameNotFoundException {
        Game game = dtoToGamesMapper.getDtoFromGame(gameDto);
        if (games.containsKey(game.getGameId())) {
            games.put(game.getGameId(), game);
        } else {
            throw new GameNotFoundException("update");
        }
    }

    @Override
    public T getById(long gameId) throws GameNotFoundException {
        Game game = null;
        if (games.containsKey(gameId)) {
            game = games.get(gameId);
        } else {
            throw new GameNotFoundException("getById. Not Found id");
        }
        return (T) game;
    }

    @Override
    public void clearAllData() {
        games.clear();
        this.countForNewGameId = 0;
    }

}
