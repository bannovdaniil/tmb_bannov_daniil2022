package com.tambov.repository.dao;


public interface Specification<T> {
    boolean isSpecified(T specification);
}
