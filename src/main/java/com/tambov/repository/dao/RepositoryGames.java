package com.tambov.repository.dao;

import com.tambov.repository.dto.GameDto;
import com.tambov.repository.exception.GameNotFoundException;
import com.tambov.repository.model.Game;

import java.util.List;

public interface RepositoryGames<T extends Game> {
    List query(Specification<T> specification);

    Game create(GameDto gameDto);

    void remove(long gameId) throws GameNotFoundException;

    void update(GameDto gameDto) throws GameNotFoundException;

    T getById(long gameId) throws GameNotFoundException;

    void clearAllData();
}
