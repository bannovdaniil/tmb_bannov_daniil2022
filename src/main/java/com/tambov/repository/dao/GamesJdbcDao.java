package com.tambov.repository.dao;

import com.tambov.repository.constant.PropertiesPath;
import com.tambov.repository.dto.GameDto;
import com.tambov.repository.exception.GameNotFoundException;
import com.tambov.repository.mapper.DtoToGamesMapper;
import com.tambov.repository.mapper.GamesToDtoMapper;
import com.tambov.repository.model.*;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

@Repository
public class GamesJdbcDao<T extends Game> implements RepositoryGames<T>, AutoCloseable {
    private final DtoToGamesMapper dtoToGamesMapper;
    private final GamesToDtoMapper gamesToDtoMapper;

    @Getter
    private Connection connection;
    private final Properties properties;

    @Autowired
    public GamesJdbcDao(DtoToGamesMapper dtoToGamesMapper, GamesToDtoMapper gamesToDtoMapper) throws Exception {
        this.dtoToGamesMapper = dtoToGamesMapper;
        this.gamesToDtoMapper = gamesToDtoMapper;
        Path fileProperties = Paths.get(PropertiesPath.DB_CONNECTION_PROPERTIES_PATH);
        properties = new Properties();
        try (var fr = new FileReader(fileProperties.toFile())) {
            properties.load(fr);
        }
        initConnection();
    }

    private void initConnection() throws Exception {
        String driver = properties.getProperty("driver");
        String url = properties.getProperty("url");
        String login = properties.getProperty("login");
        String password = properties.getProperty("password");
        Class.forName(driver);
        this.connection = DriverManager.getConnection(url, login, password);
    }

    public void createTable() {
        Path fileSqlCreateAndCleanTable = Paths.get(PropertiesPath.DB_SQl_CREATE_TABLE_FILE);
        String sql;
        try {
            sql = Files.readString(fileSqlCreateAndCleanTable, StandardCharsets.US_ASCII);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.execute();
        } catch (Exception e) {
            throw new IllegalStateException("Create table: " + e.getMessage());
        }
    }

    @Override
    public void close() throws Exception {
        if (connection != null) {
            connection.close();
        }
    }

    @Override
    public Game create(GameDto gameDto) {
        Game game = dtoToGamesMapper.getDtoFromGame(gameDto);
        long gameIdFromDb = -1;
        gameIdFromDb = addGameInformation(game, gameIdFromDb);

        if (gameIdFromDb != -1) {
            game.setGameId(gameIdFromDb);
            for (var player : game.getPlayers()) {
                long playerId = savePlayerInformation(player);
                player.setPlayerId(playerId);
                addPlayerInformation(game, player);
            }

            List<Cell> cells = game.getCells().getCellList();
            for (var cell : cells) {
                cell.setGameId(gameIdFromDb);
                saveCellInformation(cell);
            }
        }

        return game;
    }

    @Override
    public void remove(long gameId) throws GameNotFoundException {
        boolean resultPlayersDelete = deletePlayersInformation(gameId);
        boolean resultCellsDelete = deleteCellsInformation(gameId);
        boolean resultGameDelete = deleteGame(gameId);

        if (!(resultPlayersDelete & resultCellsDelete & resultGameDelete)) {
            throw new GameNotFoundException("remove. Not Found id");
        }
    }

    @Override
    public void update(GameDto gameDto) throws GameNotFoundException {
        Game game = dtoToGamesMapper.getDtoFromGame(gameDto);
        long gameIdFromDb = game.getGameId();

        if (gameIdFromDb != -1) {
            updateGameInformation(game);
            for (var player : game.getPlayers()) {
                updatePlayerInformation(player);
                updateGamePlayersInformation(game, player);
            }

            List<Cell> cells = game.getCells().getCellList();
            for (var cell : cells) {
                updateCellInformation(cell);
            }
        } else {
            throw new GameNotFoundException("remove");
        }
    }

    @Override
    public List<T> query(Specification<T> specification) {
        return getAll().
                stream().
                map(game -> (T) game).
                filter(specification::isSpecified).
                collect(Collectors.toList());
    }

    @Override
    public T getById(long gameId) throws GameNotFoundException {
        var sql = "SELECT * FROM games WHERE game_id = ? LIMIT 1;";

        Game game = null;
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, gameId);

            try (ResultSet resultSet = statement.executeQuery()) {
                if (!resultSet.next()) {
                    throw new GameNotFoundException("getById. Not Found id");
                } else {
                    if (resultSet.getInt("game_id") == gameId) {
                        var players = getGamePlayers(gameId);

                        game = new Game(
                                resultSet.getTimestamp("game_date").toLocalDateTime(),
                                resultSet.getString("game_name"),
                                resultSet.getInt("field_size"),
                                players.get(0),
                                players.get(1)
                        );
                        game.setGameId(gameId);
                        Cells cells = getCells(gameId, resultSet.getInt("field_size"));
                        game.setCells(cells);
                    }
                }
            }
        } catch (GameNotFoundException err) {
            throw new GameNotFoundException(err.getMessage());
        } catch (Exception e) {
            throw new IllegalStateException("Games by ID: " + e.getMessage());
        }

        return (T) game;
    }

    @Override
    public void clearAllData() {
        createTable();
    }

    private List<Player> getGamePlayers(long gameId) {
        var players = new ArrayList<Player>();
        var sql = "SELECT * FROM game_players WHERE game_id = ?;";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, gameId);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Player player = getPlayer(resultSet.getInt("player_id")
                            , resultSet.getString("game_char")
                            , PlayerStatus.getPlayerStatus(resultSet.getInt("player_status")));
                    players.add(player);
                }
            }
        } catch (Exception e) {
            throw new IllegalStateException("Players list select: " + e.getMessage());
        }

        return players;
    }

    private Player getPlayer(long playerID, String gameChar, PlayerStatus playerStatus) {
        var sql = "SELECT * FROM players WHERE player_id = ? LIMIT 1;";
        Player player = null;

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, playerID);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    player = new Player(
                            resultSet.getInt("player_id"),
                            resultSet.getString("player_name"),
                            resultSet.getInt("player_rate"),
                            TurnStatus.valueOf(gameChar),
                            playerStatus);
                }
            }
        } catch (Exception e) {
            throw new IllegalStateException("Player select: " + e.getMessage());
        }
        return player;
    }

    private Cells getCells(long gameId, int fieldSize) {
        var sql = "SELECT * FROM cells WHERE game_id = ? LIMIT 1;";
        Cells cells;

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, gameId);
            try (ResultSet resultSet = statement.executeQuery()) {
                cells = new Cells(fieldSize);
                while (resultSet.next()) {
                    int x = resultSet.getInt("x");
                    int y = resultSet.getInt("y");
                    cells.setCell(x, y
                            , new Cell(x, y,
                                    resultSet.getInt("turn"),
                                    TurnStatus.valueOf(resultSet.getString("status_char"))
                            ));
                }
            }
        } catch (Exception e) {
            throw new IllegalStateException("Player select: " + e.getMessage());
        }

        return cells;
    }

    private boolean deleteGame(long gameId) {
        try (PreparedStatement statement =
                     connection.prepareStatement("DELETE FROM games WHERE game_id = ?;")) {
            statement.setLong(1, gameId);
            statement.execute();
        } catch (Exception e) {
            throw new IllegalStateException("Delete game: " + e.getMessage());
        }
        return true;
    }

    private boolean deleteCellsInformation(long gameId) {
        try (PreparedStatement statement =
                     connection.prepareStatement("DELETE FROM cells WHERE game_id = ?;")) {
            statement.setLong(1, gameId);
            statement.execute();
        } catch (Exception e) {
            throw new IllegalStateException("Delete game cells: " + e.getMessage());
        }
        return true;
    }

    private boolean deletePlayersInformation(long gameId) {
        try (PreparedStatement statement =
                     connection.prepareStatement("DELETE FROM game_players WHERE game_id = ?;")) {
            statement.setLong(1, gameId);
            statement.execute();
        } catch (Exception e) {
            throw new IllegalStateException("Delete game cells: " + e.getMessage());
        }
        return true;
    }

    private void saveCellInformation(Cell cell) {
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO cells (game_id, turn, x, y, status_char) " +
                        "VALUES (?, ?, ?, ?, ?);")) {
            statement.setLong(1, cell.getGameId());
            statement.setInt(2, cell.getTurn());
            statement.setInt(3, cell.getX());
            statement.setInt(4, cell.getY());
            statement.setString(5, cell.getTurnStatus().toString());

            statement.execute();
        } catch (SQLException e) {
            throw new IllegalStateException("Cell save: " + e.getMessage());
        }
    }

    private boolean checkPlayerExists(long playerID) {
        var sql = "SELECT * FROM players WHERE player_id = ? LIMIT 1;";
        boolean resultExists = false;

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, playerID);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    resultExists = true;
                }
            }
        } catch (Exception e) {
            throw new IllegalStateException("Player select: " + e.getMessage());
        }
        return resultExists;
    }

    private long savePlayerInformation(Player player) {
        long playerId = player.getPlayerId();
        if (!checkPlayerExists(playerId)) {
            try (PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO players ( player_name, player_rate) " +
                            "VALUES (?, ?);",
                    Statement.RETURN_GENERATED_KEYS)) {
                statement.setString(1, player.getPlayerName());
                statement.setInt(2, player.getPlayerRate());

                statement.execute();

                try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        playerId = generatedKeys.getInt("player_id");
                    }
                }
            } catch (SQLException e) {
                throw new IllegalStateException("Players information save: " + e.getMessage());
            }
        }

        return playerId;
    }

    private long addPlayerInformation(Game game, Player player) {
        long GamePlayersInformationId = -1;

        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO game_players(game_id, player_id, game_char, player_status) " +
                        "VALUES (?, ?, ?, ?);",
                Statement.RETURN_GENERATED_KEYS)) {
            statement.setLong(1, game.getGameId());
            statement.setLong(2, player.getPlayerId());
            statement.setString(3, player.getGameChar().toString());
            statement.setInt(4, player.getPlayerStatus().value);

            statement.execute();

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    GamePlayersInformationId = generatedKeys.getInt("game_players_id");
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException("Players information save: " + e.getMessage());
        }
        return GamePlayersInformationId;
    }

    private long addGameInformation(Game game, long gameIdFromDb) {
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO games(game_name, game_date, field_size) " +
                        "VALUES (?, ?, ?)",
                Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, game.getGameName());
            statement.setTimestamp(2, Timestamp.valueOf(game.getGameDate()));
            statement.setInt(3, game.getFieldSize());
            statement.execute();
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    gameIdFromDb = generatedKeys.getInt("game_id");
                    game.setGameId(gameIdFromDb);
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException("Game information save: " + e.getMessage());
        }
        return gameIdFromDb;
    }

    private void updateCellInformation(Cell cell) {
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE cells " +
                        "SET " +
                        "turn = ? , " +
                        "status_char = ? " +
                        "WHERE " +
                        "game_id = ? AND " +
                        "x = ? AND " +
                        "y = ? ;")) {
            statement.setInt(1, cell.getTurn());
            statement.setString(2, cell.getTurnStatus().toString());

            statement.setLong(3, cell.getGameId());
            statement.setInt(4, cell.getX());
            statement.setInt(5, cell.getY());

            statement.execute();
        } catch (SQLException e) {
            throw new IllegalStateException("Cell update: " + e.getMessage());
        }
    }

    private void updateGamePlayersInformation(Game game, Player player) {
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE game_players " +
                        "SET " +
                        "game_char = ? , " +
                        "player_status = ? " +
                        "WHERE game_id = ? AND " +
                        "player_id = ? ;")) {

            statement.setString(1, player.getGameChar().toString());
            statement.setInt(2, player.getPlayerStatus().value);
            statement.setLong(3, game.getGameId());
            statement.setLong(4, player.getPlayerId());

            statement.execute();

        } catch (SQLException e) {
            throw new IllegalStateException("Game Players information update: " + e.getMessage());
        }
    }

    private void updatePlayerInformation(Player player) {
        long playerId = player.getPlayerId();

        if (checkPlayerExists(playerId)) {
            try (PreparedStatement statement = connection.prepareStatement(
                    "UPDATE players " +
                            "SET player_name = ?, " +
                            "player_rate = ? " +
                            "WHERE player_id = ? ;")) {
                statement.setString(1, player.getPlayerName());
                statement.setInt(2, player.getPlayerRate());
                statement.setLong(3, player.getPlayerId());
                statement.execute();

            } catch (SQLException e) {
                throw new IllegalStateException("Update player: " + e.getMessage());
            }
        } else {
            throw new IllegalStateException("Update player not found");
        }
    }

    private void updateGameInformation(Game game) {
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE games " +
                        "SET game_name = ?, " +
                        " game_date = ?, " +
                        " field_size = ? " +
                        "WHERE game_id = ?;")) {
            statement.setString(1, game.getGameName());
            statement.setTimestamp(2, Timestamp.valueOf(game.getGameDate()));
            statement.setInt(3, game.getFieldSize());
            statement.setLong(4, game.getGameId());
            statement.execute();
        } catch (SQLException e) {
            throw new IllegalStateException("Game information update: " + e.getMessage());
        }
    }

    public List<Game> getAll() {
        var games = new ArrayList<Game>();
        var sql = "SELECT * FROM games;";

        try (PreparedStatement statement =
                     connection.prepareStatement(sql)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    long gameId = resultSet.getInt("game_id");
                    var players = getGamePlayers(gameId);

                    Game game = new Game(
                            resultSet.getTimestamp("game_date").toLocalDateTime(),
                            resultSet.getString("game_name"),
                            resultSet.getInt("field_size"),
                            players.get(0),
                            players.get(1)
                    );
                    game.setGameId(gameId);
                    Cells cells = getCells(gameId, resultSet.getInt("field_size"));
                    game.setCells(cells);
                    games.add(game);
                }
            }
        } catch (Exception e) {
            throw new IllegalStateException("Games list select: " + e.getMessage());
        }

        return games;
    }

}
