package com.tambov.repository.dao;

import com.tambov.repository.model.Game;

import java.util.function.Predicate;

public class GamesSpecification<T extends Game> implements Specification<T> {
    private final Predicate<T> predicate;

    public GamesSpecification(Predicate<T> predicate) {
        this.predicate = predicate;
    }

    @Override
    public boolean isSpecified(T specification) {
        return predicate.test(specification);
    }
}
