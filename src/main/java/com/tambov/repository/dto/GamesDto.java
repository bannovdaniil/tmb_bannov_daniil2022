package com.tambov.repository.dto;

import java.util.HashMap;
import java.util.Map;

public class GamesDto {
    public final Map<Long, GameDto> games = new HashMap<>();
    public long countForNewGameId;

}
