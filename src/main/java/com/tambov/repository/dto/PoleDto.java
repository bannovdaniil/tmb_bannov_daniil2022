package com.tambov.repository.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.Arrays;

public class PoleDto {
    @Size(min = 3, max = 5, message = "poleSize must be between 3 and 5")
    public int poleSize;
    public CellDto[][] cells;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PoleDto poleDto = (PoleDto) o;

        if (poleSize != poleDto.poleSize) return false;
        return Arrays.deepEquals(cells, poleDto.cells);
    }

    @Override
    public int hashCode() {
        int result = poleSize;
        result = 31 * result + Arrays.deepHashCode(cells);
        return result;
    }
}
