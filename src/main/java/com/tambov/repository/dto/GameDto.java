package com.tambov.repository.dto;

import com.tambov.repository.constant.DateCheckerConstant;
import com.tambov.repository.constant.RegExConstant;
import com.tambov.repository.validation.CustomDateChecker;

import javax.validation.constraints.*;
import java.time.LocalDateTime;
import java.util.List;

public class GameDto {
    @Min(value = 0, message = "ID must be greater or equals 0")
    public long gameId;
    @NotNull(message = "gameDate is Null")
    @CustomDateChecker(value = DateCheckerConstant.CUSTOM_DATE
            , message = DateCheckerConstant.CUSTOM_DATE_ERROR_MESSAGE)
    public LocalDateTime gameDate;
    @NotEmpty(message = "Name should not be empty")
    @Size(min = 2, max = 50, message = "Name size must be between 2 and 50 characters")
    @Pattern(regexp = RegExConstant.UPPER_CASE_FIRST_LETTER_AZ
            , message = "Name must start with [A-Z] Uppercase character.")
    public String name;
    public int fieldSize;
    @Min(value = 1, message = "Round must be greater or equals 1")
    public int round;
    public PoleDto pole;
    public List<PlayerDto> players;

}
