package com.tambov.repository.dto;

import com.tambov.repository.model.PlayerStatus;
import com.tambov.repository.model.TurnStatus;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class PlayerDto {
    public long playerId;
    @NotEmpty(message = "Name should not be empty")
    @Size(min = 2, max = 50, message = "Name size must be between 2 and 50 characters")
    public String playerName;
    @Min(value = 0, message = "Score must be greater or equals 0")
    public int playerRate;
    public TurnStatus gameChar;
    public PlayerStatus playerStatus;

}
