package com.tambov.repository.dto;

import com.tambov.repository.model.TurnStatus;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class CellDto {
    @Size(min = 0, max = 4, message = "X must be between 0 and 4")
    public int x;
    @Size(min = 0, max = 4, message = "Y must be between 0 and 4")
    public int y;
    @Pattern(regexp = "^[O|X]$|^NULL$")
    public TurnStatus turnStatus;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CellDto cellDto = (CellDto) o;

        if (x != cellDto.x) return false;
        if (y != cellDto.y) return false;
        return turnStatus == cellDto.turnStatus;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        result = 31 * result + (turnStatus != null ? turnStatus.hashCode() : 0);
        return result;
    }
}
