package com.tambov.repository.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Cells {
    private int fieldSize;
    private Cell[][] cells;

    public Cells() {
    }

    public Cells(int fieldSize) {
        this.fieldSize = fieldSize;
        cells = new Cell[fieldSize][];
        for (int x = 0; x < fieldSize; x++) {
            cells[x] = new Cell[fieldSize];
            for (int y = 0; y < fieldSize; y++) {
                cells[x][y] = new Cell(x, y);
            }
        }
    }

    public int getFieldSize() {
        return fieldSize;
    }

    public Cell[][] getCells() {
        return cells;
    }

    public List<Cell> getCellList() {
        List<Cell> cellList = new ArrayList<>();

        for (int x = 0; x < fieldSize; x++) {
            for (int y = 0; y < fieldSize; y++) {
                cellList.add(cells[x][y]);
            }
        }

        return cellList;
    }

    public void setCellStatus(int x, int y, TurnStatus turnStatus) {
        if (turnStatus != TurnStatus.N) {
            this.cells[x][y].setTurnStatus(turnStatus);
        }
    }

    public void setCell(int x, int y, Cell cell) {
        this.cells[x][y] = cell;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cells cells = (Cells) o;

        if (fieldSize != cells.fieldSize) return false;
        return Arrays.deepEquals(this.cells, cells.cells);
    }

    @Override
    public int hashCode() {
        int result = fieldSize;
        result = 31 * result + Arrays.deepHashCode(cells);
        return result;
    }
}
