package com.tambov.repository.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Game {
    private long gameId;
    private LocalDateTime gameDate;
    private String gameName;
    private int fieldSize;
    private Cells cells;
    private List<Player> players;

    public Game() {
        this.gameId = -1;
    }

    public Game(int gameId) {
        this.gameId = gameId;
    }

    public Game(String gameName, int poleSize, Player player1, Player player2) {
        this.gameId = -1;
        this.gameName = gameName;
        this.cells = new Cells(poleSize);
        this.players = new ArrayList<>();
        this.players.add(player1);
        this.players.add(player2);
    }

    public Game(LocalDateTime gameDate, String gameName, int poleSize, Player player1, Player player2) {
        this.gameId = -1;
        this.gameDate = gameDate;
        this.gameName = gameName;
        this.cells = new Cells(poleSize);
        this.players = new ArrayList<>();
        this.players.add(player1);
        this.players.add(player2);
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public int getFieldSize() {
        return fieldSize;
    }

    public void setFieldSize(int fieldSize) {
        this.fieldSize = fieldSize;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public LocalDateTime getGameDate() {
        return gameDate;
    }

    public void setGameDate(LocalDateTime gameDate) {
        this.gameDate = gameDate;
    }

    public long getGameId() {
        return gameId;
    }

    public void setGameId(long gameId) {
        this.gameId = gameId;
    }

    public Cells getCells() {
        return cells;
    }

    public void setCells(Cells cells) {
        this.cells = cells;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public String getGameName() {
        return gameName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Game game = (Game) o;

        if (gameDate != null ? !gameDate.equals(game.gameDate) : game.gameDate != null) return false;
        return gameName != null ? gameName.equals(game.gameName) : game.gameName == null;
    }

    @Override
    public int hashCode() {
        int result = gameDate != null ? gameDate.hashCode() : 0;
        result = 31 * result + (gameName != null ? gameName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + gameId +
                ", name=" + gameName +
                '}';
    }
}
