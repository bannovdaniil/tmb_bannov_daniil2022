package com.tambov.repository.model;

public enum PlayerStatus {
    LOSE(0),
    WIN(1),
    PLAY(2);

    public final int value;

    PlayerStatus(int value) {
        this.value = value;

    }

    private static PlayerStatus[] valuesName = PlayerStatus.values();

    public static PlayerStatus getPlayerStatus(int value) {
        return valuesName[value];
    }

}
