package com.tambov.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Player {
    private long playerId;
    private String playerName;
    private int playerRate;
    private TurnStatus gameChar;
    private PlayerStatus playerStatus;

    public Player(long playerId, String playerName, int playerRate, TurnStatus gameChar, PlayerStatus playerStatus) {
        this.playerId = playerId;
        this.playerName = playerName;
        this.playerRate = playerRate;
        this.gameChar = gameChar;
        this.playerStatus = playerStatus;
    }

    public Player(String playerName) {
        this.playerName = playerName;
        this.playerRate = 0;
        this.gameChar = TurnStatus.N;
        this.playerStatus = PlayerStatus.PLAY;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        return playerName != null ? playerName.equals(player.playerName) : player.playerName == null;
    }

    @Override
    public int hashCode() {
        return playerName != null ? playerName.hashCode() : 0;
    }
}
