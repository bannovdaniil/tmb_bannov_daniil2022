package com.tambov.repository.model;

import lombok.Getter;

@Getter
public class Cell {
    private int cellId;
    private long gameId;
    private int x;
    private int y;
    private int turn;
    private TurnStatus turnStatus;

    public Cell(int x, int y) {
        this.x = x;
        this.y = y;
        this.turn = 0;
        this.turnStatus = TurnStatus.N;
    }

    public Cell(int x, int y, int turn, TurnStatus turnStatus) {
        this.x = x;
        this.y = y;
        this.turn = turn;
        this.turnStatus = turnStatus;
    }

    public void setTurnStatus(TurnStatus turnStatus) {
        this.turnStatus = turnStatus;
    }

    public void setGameId(long gameId) {
        this.gameId = gameId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cell cell = (Cell) o;

        if (cellId != cell.cellId) return false;
        if (gameId != cell.gameId) return false;
        if (x != cell.x) return false;
        if (y != cell.y) return false;
        if (turn != cell.turn) return false;
        return turnStatus == cell.turnStatus;
    }

    @Override
    public int hashCode() {
        int result = cellId;
        result = 31 * result + (int) (gameId ^ (gameId >>> 32));
        result = 31 * result + x;
        result = 31 * result + y;
        result = 31 * result + turn;
        result = 31 * result + (turnStatus != null ? turnStatus.hashCode() : 0);
        return result;
    }
}
