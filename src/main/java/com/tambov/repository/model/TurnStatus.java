package com.tambov.repository.model;

public enum TurnStatus {
    O,
    X,
    N
}
