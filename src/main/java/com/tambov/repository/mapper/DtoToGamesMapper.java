package com.tambov.repository.mapper;

import com.tambov.repository.dto.CellDto;
import com.tambov.repository.dto.GameDto;
import com.tambov.repository.dto.PlayerDto;
import com.tambov.repository.model.Cells;
import com.tambov.repository.model.Game;
import com.tambov.repository.model.Player;
import org.springframework.stereotype.Controller;

@Controller
public class DtoToGamesMapper {

    public Game getDtoFromGame(GameDto gameDto) {
        PlayerDto playerDto = gameDto.players.get(0);
        Player player1 = new Player(playerDto.playerId
                , playerDto.playerName
                , playerDto.playerRate
                , playerDto.gameChar
                , playerDto.playerStatus);

        playerDto = gameDto.players.get(1);
        Player player2 = new Player(playerDto.playerId
                , playerDto.playerName
                , playerDto.playerRate
                , playerDto.gameChar
                , playerDto.playerStatus);

        int poleSize = gameDto.pole.poleSize;
        Game game = new Game(gameDto.gameDate, gameDto.name,
                poleSize,
                player1,
                player2);
        game.setGameId(gameDto.gameId);
        Cells cells = getPoleFromDtoCell(poleSize, gameDto.pole.cells);
        game.setCells(cells);
        game.setFieldSize(gameDto.fieldSize);
        return game;
    }

    private Cells getPoleFromDtoCell(int poleSize, CellDto[][] cellsDto) {
        Cells cells = new Cells(poleSize);
        for (int x = 0; x < poleSize; x++) {
            for (int y = 0; y < poleSize; y++) {
                cells.setCellStatus(
                        cellsDto[x][y].x,
                        cellsDto[x][y].y,
                        cellsDto[x][y].turnStatus);
            }
        }
        return cells;
    }
}
