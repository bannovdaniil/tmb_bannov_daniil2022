package com.tambov.repository.mapper;

import com.tambov.repository.dto.CellDto;
import com.tambov.repository.dto.GameDto;
import com.tambov.repository.dto.PlayerDto;
import com.tambov.repository.dto.PoleDto;
import com.tambov.repository.model.Cell;
import com.tambov.repository.model.Game;
import com.tambov.repository.model.Player;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;

@Controller
public class GamesToDtoMapper {

    public GameDto getGameDto(Game game) {
        if (game == null) {
            return null;
        }
        GameDto gameDto = new GameDto();
        gameDto.gameId = game.getGameId();
        gameDto.gameDate = game.getGameDate();
        gameDto.name = game.getGameName();
        gameDto.fieldSize = game.getFieldSize();

        gameDto.pole = new PoleDto();
        int poleSize = game.getCells().getFieldSize();
        gameDto.pole.poleSize = poleSize;
        gameDto.pole.cells = new CellDto[poleSize][poleSize];
        gameDto.pole.cells = getCellDtos(game, poleSize);

        gameDto.players = new ArrayList<>();
        for (var player : game.getPlayers()) {
            PlayerDto playerDto = getPlayerDto(player);
            gameDto.players.add(playerDto);
        }
        return gameDto;
    }

    private CellDto[][] getCellDtos(Game game, int poleSize) {
        CellDto[][] cellsDto = new CellDto[poleSize][];
        Cell[][] cells = game.getCells().getCells();
        for (int x = 0; x < poleSize; x++) {
            cellsDto[x] = new CellDto[poleSize];
            for (int y = 0; y < poleSize; y++) {
                cellsDto[x][y] = new CellDto();
                cellsDto[x][y].x = x;
                cellsDto[x][y].y = y;
                cellsDto[x][y].turnStatus = cells[x][y].getTurnStatus();
            }
        }
        return cellsDto;
    }

    private PlayerDto getPlayerDto(Player player) {
        PlayerDto playerDto = new PlayerDto();
        playerDto.playerId = player.getPlayerId();
        playerDto.playerName = player.getPlayerName();
        playerDto.playerRate = player.getPlayerRate();
        playerDto.playerStatus = player.getPlayerStatus();
        playerDto.gameChar = player.getGameChar();
        return playerDto;
    }
}
