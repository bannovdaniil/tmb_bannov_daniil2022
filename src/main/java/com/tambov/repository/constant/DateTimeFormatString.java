package com.tambov.repository.constant;

public class DateTimeFormatString {
    public final static String DATE_TIME_FORMATTER = "yyyy-MM-dd HH:mm:ss";
}
