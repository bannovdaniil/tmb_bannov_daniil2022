package com.tambov.repository.constant;

import java.io.File;

public final class PropertiesPath {
    public static final String DB_CONNECTION_PROPERTIES_PATH
            = String.format("src%1$smain%1$sresources%1$sapp.properties", File.separator);
    public static final String DB_SQl_CREATE_TABLE_FILE
            = String.format("src%1$smain%1$sresources%1$sscheme%1$s1_create_table.sql", File.separator);
}
