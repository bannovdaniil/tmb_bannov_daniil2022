package com.tambov.repository.constant;

public final class RegExConstant {
    public final static String UPPER_CASE_FIRST_LETTER_AZ = "[A-Z]+(.*)";
}
