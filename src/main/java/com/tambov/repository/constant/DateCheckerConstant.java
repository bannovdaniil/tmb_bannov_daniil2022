package com.tambov.repository.constant;

public final class DateCheckerConstant {
    public final static String CUSTOM_DATE = "2021-07-20 12:00:00";
    public final static String CUSTOM_DATE_ERROR_MESSAGE = "Data must be after " + CUSTOM_DATE;
}
