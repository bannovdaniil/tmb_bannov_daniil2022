package com.tambov.repository.controller;

import com.tambov.repository.dto.*;
import com.tambov.repository.exception.GameNotFoundException;
import com.tambov.repository.model.Game;
import com.tambov.repository.dao.GamesSpecification;
import com.tambov.repository.dao.RepositoryGames;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/games")
public class GameController {
    private final RepositoryGames gamesRepository;

    @Autowired
    public GameController(RepositoryGames gamesRepository) {
        this.gamesRepository = gamesRepository;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Game> getAllRecords() {
        return gamesRepository.query(new GamesSpecification<>(record -> record.getGameId() >= 0));
    }

    @PostMapping
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public Game createGame(@RequestBody @Valid GameDto dtoGame) {
        return gamesRepository.create(dtoGame);
    }

    @GetMapping("/{id:[\\d]+}")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Game getRecordById(@PathVariable("id") Long gameId) throws GameNotFoundException {
        return (Game) gamesRepository.query(new GamesSpecification<>(record -> record.getGameId() == gameId)).get(0);
    }

    @DeleteMapping("{id:[\\d]+}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeUser(@PathVariable("id") Long gameId) throws GameNotFoundException {
        gamesRepository.remove(gameId);
    }

    @PutMapping
    public void updateGame(@RequestBody @Valid GameDto dtoGame) throws GameNotFoundException {
        gamesRepository.update(dtoGame);
    }
}
