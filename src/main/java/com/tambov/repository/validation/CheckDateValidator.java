package com.tambov.repository.validation;

import com.tambov.repository.constant.DateTimeFormatString;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CheckDateValidator
        implements ConstraintValidator<CustomDateChecker, LocalDateTime> {

    private LocalDateTime startDate;
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DateTimeFormatString.DATE_TIME_FORMATTER);

    @Override
    public void initialize(CustomDateChecker customDateChecker) {
        ConstraintValidator.super.initialize(customDateChecker);
        this.startDate = LocalDateTime.parse(customDateChecker.value(), dateTimeFormatter);
    }

    @Override
    public boolean isValid(LocalDateTime localDateTime,
                           ConstraintValidatorContext constraintValidatorContext) {
        if (localDateTime == null) {
            return false;
        }
        return localDateTime.isAfter(this.startDate);
    }
}

